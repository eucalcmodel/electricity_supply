# Changelog
### added
- 5.1 Electricity - Climate interface
--------------------------------------- up from here the changelog is detailed
## 1.65
### Added
- Gen_calc metanode
### Changed
- ELC_electricity_fixed_assumptions: Including Climate lever level counting

## 1.6
### Added
- Cost calculation

## 1.4
### Added
- OTS electricity supply and emission calculations
- Garph for Emission and Supply
- Interface for balancing/storage module
### Changed
- 5% own use for coal, gas, oil and nuclear
- Capacity factor read-in method, changes from 2015 are 0%
- Back to the % based CCS calculations
### Removed
### Fixed 
- Python code output creation
## 0.9
- Initial version, integrating all modules